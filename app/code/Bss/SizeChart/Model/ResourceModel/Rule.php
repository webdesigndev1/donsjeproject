<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_ProductCustomTabs
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Model\ResourceModel;

use \Psr\Log\LoggerInterface;

class Rule extends \Magento\Rule\Model\ResourceModel\AbstractResource
{

    /**
     * Date model
     *
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $date;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    private $categoryFactory;

    /**
     *
     * @var \Bss\ProductCustomTabs\Model\RuleFactory
     */
    private $autoRelatedRuleFac;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    private $cart;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    private $productFactory;

    /**
     * LoggerInterface
     * @var LoggerInterface $logger
     */
    protected $logger;

    /**
     * Rule constructor.
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Customer\Model\SessionFactory $customerSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Bss\ProductCustomTabs\Model\RuleFactory $autoRelatedRuleFac
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Customer\Model\SessionFactory $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Bss\SizeChart\Model\RuleFactory $autoRelatedRuleFac,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->date = $date;
        $this->customerSession = $customerSession;
        $this->storeManager = $storeManager;
        $this->categoryFactory = $categoryFactory;
        $this->autoRelatedRuleFac = $autoRelatedRuleFac;
        $this->cart = $cart;
        $this->productFactory = $productFactory;
        $this->logger = $logger;
    }

    /**
     * Initialize main table and table id field
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('bss_rule', 'rule_id');
    }

    public function getRuleBySizeChartId($sizeChartId)
    {
        try {
            $table = $this->getTable('bss_sc_rule');
            $sql = $this->getConnection()->select()->from(
                $table,
                ['rule_id']
            )->where('size_chart_id = ?', $sizeChartId);

            $ruleId = $this->getConnection()->fetchRow($sql);

        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
        return $ruleId['rule_id'];
    }

    public function insertDataRule($ruleId, $sizeChartId)
    {
        try {
            $adapter = $this->getConnection();
            $table = $this->getTable('bss_sc_rule');
            $adapter->insert(
                $table,
                [
                    'rule_id' => $ruleId,
                    'size_chart_id' => $sizeChartId
                ]
            );
        } catch (\Exception $exception) {
            $this->logger->debug($exception->getMessage());
        }
    }

}
