<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\SizeChart\Block;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Catalog\Model\Product;
use Bss\SizeChart\Model\SizeChartFactory;
use Magento\Cms\Model\Template\FilterProvider;
use Bss\SizeChart\Helper\Data;
use Bss\SizeChart\Model\ResourceModel;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;

class SizeChartTab extends Template
{
    /**
     * Resource Size Chart
     * @var ResourceModel\SizeChart
     */
    protected $resourceSizeChart;

    /**
     * Attribute
     * @var Attribute
     */
    protected $eavAttribute;

    /**
     * FilterProvider
     *
     * @var FilterProvider
     */
    protected $filterProvider;

    /**
     * Registry
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * SizeChart
     *
     * @var SizeChartFactory
     */
    protected $sizeChartLoader;

    /**
     * Product
     *
     * @var Product
     */
    protected $productLoader;

    /**
     * ScopeConfigInterface
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Data
     *
     * @var Data
     */
    protected $helper;

    /**
     * SizeChartTab constructor.
     * @param Template\Context $context
     * @param Registry $registry
     * @param Product $productLoader
     * @param SizeChartFactory $sizeChartLoader
     * @param FilterProvider $filterProvider
     * @param Data $helper
     * @param Attribute $eavAttribute
     * @param ResourceModel\SizeChart $resourceSizeChart
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Registry $registry,
        Product $productLoader,
        SizeChartFactory $sizeChartLoader,
        FilterProvider $filterProvider,
        Data $helper,
        Attribute $eavAttribute,
        ResourceModel\SizeChart $resourceSizeChart,
        array $data
    ) {
        $this->resourceSizeChart = $resourceSizeChart;
        $this->eavAttribute = $eavAttribute;
        $this->filterProvider = $filterProvider;
        $this->coreRegistry = $registry;
        $this->productLoader = $productLoader;
        $this->sizeChartLoader = $sizeChartLoader;
        $this->scopeConfig = $context->getScopeConfig();
        $this->helper = $helper;
        parent::__construct($context, $data);
    }

    /**
     * Edit Content Size Chart
     * @param $stringContent
     * @return string
     * @throws \Exception
     */
    public function editContentSizeChart($stringContent)
    {
        return $this->filterProvider->getPageFilter()->filter($stringContent);
    }

    /**
     * @param $productId
     * @param $storeId
     * @return \Bss\SizeChart\Model\SizeChart
     */
    public function getAllSizeChartByStoreView($productId, $storeId)
    {
        try {
            $sizeChartId = $this->resourceSizeChart->getSizeChartsByStoreView($productId, $storeId);

            $productId = $this->getProduct()->getId();
            $sizeChartAttributeStore = $this->getSizeChartProductIdByStore($productId, $storeId);
            $sizeChartAttributeAllStore = $this->getSizeChartProductIdByStore($productId, 0);

            $sizeChartAttributeId = $sizeChartAttributeStore;
            if ($sizeChartAttributeId == false) {
                $sizeChartAttributeId = $sizeChartAttributeAllStore;
            }

            $sizeChartAttribute = $this->sizeChartLoader->create()->load($sizeChartAttributeId);
            $sizeChart = $this->sizeChartLoader->create()->load($sizeChartId);
            if ($sizeChart->getData('override_product_setting') == 1) {
                $sizeChartAttribute = $sizeChart;
            }
            return $sizeChartAttribute;
        } catch (\Exception $exception) {
            return null;
        }
    }

    /**
     * Get Size Chart Product Id By Store
     * @param $idProduct
     * @param $idStore
     * @return array
     */
    public function getSizeChartProductIdByStore($idProduct, $idStore)
    {
        $table = $this->resourceSizeChart->getTable('catalog_product_entity_varchar');
        $attributeId = $this->eavAttribute->getIdByCode(
            'catalog_product',
            'bss_sizechart'
        );

        $sql = $this->resourceSizeChart->buildSql($table, $idProduct, $attributeId, $idStore);

        return $this->resourceSizeChart->getConnection()->fetchRow($sql);
    }

    /**
     * Get Product
     * @return Product
     */
    public function getProduct()
    {
        if (!$this->hasData('product')) {
            $this->setData('product', $this->coreRegistry->registry('product'));
        }
        $product = $this->getData('product');

        return $product;
    }

    /**
     * Get Store Id
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }

    /**
     * Check StoreView Size Chart vs Store
     * @param int $idStoreSizeChart
     * @param int $idStoreFront
     * @return bool
     */
    public function checkStore($idStoreSizeChart, $idStoreFront)
    {
        $arrayIdStoreSizeChart = explode(',', $idStoreSizeChart);
        return in_array($idStoreFront, $arrayIdStoreSizeChart);
    }

    /**
     * Check Display Size Chart
     * @param int $idDisplaySizeChart
     * @param int $idDisplayPostion
     * @return bool
     */
    public function checkDisplay($idDisplaySizeChart, $idDisplayPostion)
    {
        $arrIdDisplay = explode(',', $idDisplaySizeChart);
        return in_array($idDisplayPostion, $arrIdDisplay);
    }

    /**
     * Get Title Admin Config
     * @return string
     */
    public function getTitle()
    {
        return $this->helper->getConfigTitle();
    }

    /**
     * Get Id Product
     * @return int
     */
    public function getId()
    {
        return $this->getProduct()->getId();
    }

    /**
     * Get Product By Id
     * @param int $id
     * @return Product
     */
    public function getProductById($id)
    {
        return $this->productLoader->load($id);
    }

    /**
     * Get Size Chart By Id
     * @param $id
     * @return \Bss\SizeChart\Model\SizeChart
     */
    public function getSizeChartById($id)
    {
        return $this->sizeChartLoader->create()->load($id);
    }

}
