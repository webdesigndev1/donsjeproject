/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at thisURL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SizeChart
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
define([
    "jquery",
    'Magento_Ui/js/modal/modal'
], function($,modal) {
    var options = {
        type: 'popup',
        responsive: true,
        innerScroll: true,
        buttons: [{
            text: $.mage.__('Back'),
            class: '',
            click: function () {
                this.closeModal();
            }
        }]
    };
     modal(options, $('#myModel'));
    $("#openModel").on("click",function(){
        $('#myModel').modal('openModal');
    });
});
